function swap (data, i, j) {
  const tmp = data[i]
  data[i] = data[j]
  data[j] = tmp
}

function siftUp (data, compare) {
  // start from the end
  let i = data.length - 1

  while (i > 0) {
    // TODO test also checking for parent
    if (compare(data[i], data[parent(i)])) {
      swap(data, i, parent(i))
      i = parent(i)
    } else {
      break
    }
  }
}

function siftDown (data, compare) {
  // starting from the root
  let i = 0

  while (i < data.length) {
    // find the minimum item
    let minimum = i

    // compare left
    // TODO test only if the left is there
    if (data[left(i)] && compare(data[left(i)], data[minimum])) {
      minimum = left(i)
    }
    // compare right
    // TODO test only if the right is there
    if (data[right(i)] && compare(data[right(i)], data[minimum])) {
      minimum = right(i)
    }

    if (minimum !== i) {
      // swap the current item with the minimum item
      swap(data, i, minimum)
      // start again from the minimum item
      i = minimum
    } else {
      // stop when no swap is needed
      break
    }
  }
}

function parent (i) {
  return Math.floor(i / 2)
}

function left (i) {
  return ((i + 1) * 2) - 1
}

function right (i) {
  return left(i) + 1
}

function heapify (data, compare = BinaryHeap.defaultComparator, i = 0) {
  if (left(i) < data.length) {
    if (!compare(data[i], data[left(i)])) {
      swap(data, i, left(i))
    }
    heapify(data, compare, left(i))
  }

  if (right(i) < data.length) {
    if (!compare(data[i], data[right(i)])) {
      swap(data, i, right(i))
    }
    heapify(data, compare, right(i))
  }

  return data
}

class BinaryHeap {
  constructor ({ data = [], compare = BinaryHeap.defaultComparator } = {}) {
    this._compare = compare
    this._data = heapify(data, compare)
  }

  push (item) {
    // when pushing, add an item at the end of the queue and sift up
    this._data.push(item)
    siftUp(this._data, this._compare)
    return this
  }

  pop () {
    if (this.size() === 0) {
      throw new Error('BinaryHeap: there are no items in the heap')
    }
    // when removing, remove an item in position n and sift down
    const element = this._data.shift()
    const last = this._data.pop()
    // TODO test this
    if (last) {
      this._data.unshift(last)
      siftDown(this._data, this._compare)
    }
    return element
  }

  peek () {
    if (this.size() === 0) {
      throw new Error('BinaryHeap: there are no items in the heap')
    }
    return this._data.slice()[0]
  }

  size () {
    return this._data.length
  }

  isEmpty () {
    return this._data.length === 0
  }
}

BinaryHeap.heapify = heapify
BinaryHeap.defaultComparator = (a, b) => a < b

module.exports = BinaryHeap
