/* eslint-env mocha */
const assert = require('assert')
const Encoder = require('./Encoder')

describe('Encoder', () => {
  it('decodes a previoulsly encoded text', () => {
    const text = 'this is a test'

    const dictionary = Encoder.createDictionary(text)
    const { encoded } = Encoder.encodeHuffman(text, dictionary)
    const { decoded } = Encoder.decodeHuffman(encoded, dictionary)

    assert.strictEqual(
      decoded,
      text
    )
  })

  it('encodes', () => {
    const text = 'aabbc'
    const dictionary = Encoder.createDictionary(text)
    assert.deepStrictEqual(
      Encoder.encodeHuffman(text, dictionary).encoded,
      '00111110'
    )
  })

  it('decodes', () => {
    const text = 'aabbc'
    const encoded = '00111110'
    const dictionary = Encoder.createDictionary(text)
    const { decoded } = Encoder.decodeHuffman(encoded, dictionary)

    assert.deepStrictEqual(
      decoded,
      'aabbc'
    )
  })
})
