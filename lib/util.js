const __debug__ = {}

module.exports.debug = function debug (id) {
  return function (...messages) {
    if (!process.env.DEBUG) {
      return true
    }
    const previous = __debug__[id]
    let delta = 0
    if (previous) {
      delta = Date.now() - previous
      __debug__[id] = Date.now()
    } else {
      __debug__[id] = Date.now()
    }

    if (messages.length > 0) {
      console.log('[' + id + ']: ' + String(delta).padStart(3, '0') + 'ms\t', ...messages)
    }
  }
}
