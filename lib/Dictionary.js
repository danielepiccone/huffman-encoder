const Dictionary = {}

Dictionary.create = (dictionary) => {
  Object.keys(Object.assign({}, dictionary)).forEach(key => {
    const value = dictionary[key]
    const padding = value.length
    const int = parseInt(value, 2)
    dictionary[key] = `${int}:${padding}`
  })

  return JSON.stringify(dictionary)
}

Dictionary.parse = (encoded) => {
  const dictionary = JSON.parse(encoded)
  Object.keys(dictionary).forEach(key => {
    const value = dictionary[key]
    const [int, padding] = value.split(':')
    dictionary[key] = parseInt(int, 10).toString(2).padStart(padding, '0')
  })
  return dictionary
}

module.exports = Dictionary
