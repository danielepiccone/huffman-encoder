/* eslint-env mocha */
const assert = require('assert')
const fs = require('fs')
const path = require('path')

const { decodeHuffmanStream } = require('./EncoderStream')

const consumeStream = (stream, callback) => new Promise(resolve => {
  let data

  stream.on('data', chunk => {
    data = data ? Buffer.concat([data, chunk]) : chunk
  })

  stream.on('end', () => resolve(data))
})

describe('EncoderStream', () => {
  it('decodes a previoulsly encoded stream from the header to th code', async () => {
    const original = await consumeStream(fs.createReadStream(path.resolve(__dirname, '../fixtures/pasta.txt')))
    const decoded = await consumeStream(fs.createReadStream(path.resolve(__dirname, '../fixtures/pasta.txt.huf')).pipe(decodeHuffmanStream))
    assert.strictEqual(
      original.toString(),
      decoded.toString()
    )
  })
})
