const BYTE = 8

class BitSeq {
  constructor () {
    this._size = 0
    this._bits = []
  }

  size () {
    return this._size.valueOf()
  }

  /**
   * Return a string representation of the bit sequence
   *
   * @return {String}
   **/
  toString () {
    const buffer = this.read()
    let result = ''
    for (let i = 0; i < buffer.length; i++) {
      result = result + buffer.readUInt8(i).toString(2).padStart(BYTE, '0')
    }
    return result.substring(0, this._size)
  }

  /**
   * write bits as string to the sequence
   *
   * @param {string} bits
   * @return {Buffer}
   **/
  write (bits) {
    if (typeof bits !== 'string') {
      throw new Error('BitSeq: invalid parameter')
    }

    while (bits.length > 0) {
      const tail = this._size % BYTE
      const chunk = bits.substr(0, BYTE)

      let bitsToAppend
      if (tail > 0) {
        // some bits are in the last byte, take them
        const partialByte = this._bits.pop()

        // get the bits from the input with padding at the end (for parsing to int)
        bitsToAppend = parseInt(chunk.padEnd(BYTE, '0'), 2)
        // shift the bits to make space for the partialByte
        bitsToAppend = bitsToAppend >> tail
        // union between the partial byte and the the input
        bitsToAppend = bitsToAppend | partialByte

        this._bits.push(bitsToAppend)
      } else {
        // consider the bits start from a clean byte
        // add zero padding to the end, to complete the last byte
        const paddedBits = chunk.padEnd(BYTE, '0')
        bitsToAppend = parseInt(paddedBits, 2)
        this._bits.push(bitsToAppend)
      }

      // the size should always increase by the input length
      this._size = this._size + chunk.length

      bits = bits.substr(BYTE)
    }
    return this
  }

  /**
   * read bits as bytes from the sequence
   *
   * @param {Number} offset
   * @param {Number} limit
   * @return {Buffer}
   **/
  read (offset, limit) {
    const data = this._bits.slice(offset, limit)
    return Buffer.from(data)
  }

  /**
   * read all the sequence and clears the buffer
   *
   * @return {Buffer}
   **/
  flush () {
    const bits = this._bits
    this._bits = []
    return Buffer.from(bits)
  }
}

/**
 * create a BitSeq from a string representing the bits
 *
 * @param {string} bits
 * @return {BitSeq}
 **/
BitSeq.fromString = function fromStrin (bits) {
  return new BitSeq().write(bits)
}

/**
 * create a BitSeq from a buffer an a size in bits
 *
 * @param {Buffer} buffer
 * @param {Number} size - the number of bits to consider
 * @return {BitSeq}
 **/

BitSeq.fromBuffer = function fromBuffer (buffer, size) {
  if (!size) {
    size = buffer.length * BYTE
  }

  const bitSeq = new BitSeq()

  for (let byte of buffer.values()) {
    bitSeq._bits.push(byte)
  }

  bitSeq._size = size

  return bitSeq
}

module.exports = BitSeq
