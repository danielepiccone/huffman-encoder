/* eslint-env mocha */
const assert = require('assert')
const Header = require('./Header')

describe('Header', () => {
  it('encodes the signature, version, dictionary size and encoded size', () => {
    const dictionary = {
      foo: '1010',
      bar: '1110'
    }

    const encoded = '11101010'

    const headerBuffer = Header.create({ dictionary, encoded })

    const { version, dictSize, codeSize } = Header.parse(headerBuffer)

    assert.strictEqual(version, 1)
    assert.strictEqual(dictSize, JSON.stringify(dictionary).length)
    assert.strictEqual(codeSize, encoded.length)
  })
})
