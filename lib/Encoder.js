const BinaryHeap = require('./BinaryHeap.js')
const assert = require('assert')
const debug = require('./util').debug('Encoder')

function createProbabilityMap (inputText) {
  let probabilityMap = {}
  let text = inputText

  while (text.length > 0) {
    const char = text[0]
    let previousProbability = (probabilityMap[char] || 0)
    probabilityMap[char] = previousProbability + (1 / inputText.length)
    text = text.substr(1)
  }

  return probabilityMap
}

function createDictionary (text) {
  const probabilityMap = createProbabilityMap(text)
  const heap = new BinaryHeap({
    data: Object.entries(probabilityMap),
    compare: (a, b) => a[1] < b[1]
  })

  let tree
  while (true) {
    let min1 = heap.pop()
    let min2 = heap.pop()

    tree = [
      min1[0] + min2[0], // concatenate chars
      min1[1] + min2[1], // sum probabilities
      [ min1, min2 ] // both are children
    ]

    if (heap.isEmpty()) {
      break
    } else {
      heap.push(tree)
    }
  }

  // the top probability must be 1
  assert(tree[1], 1)

  // build the map
  let dictionary = {}

  const walknode = (char, path = '') => (node) => {
    if (typeof node[2] === 'undefined') {
      return path
    }

    if (node[2][0][0].includes(char)) {
      return walknode(char, path + '0')(node[2][0])
    }

    if (node[2][1][0].includes(char)) {
      return walknode(char, path + '1')(node[2][1])
    }
  }

  let allChars = tree[0]
  for (let char of allChars) {
    dictionary[char] = walknode(char)(tree)
  }

  return dictionary
}

function encodeHuffman (text, dictionary) {
  assert(typeof text === 'string', 'the first parameter must be a string')
  assert(typeof dictionary === 'object', 'the second parameter must be an object')

  debug('Start encoding')

  let code = ''
  while (text.length > 0) {
    let token = text[0]
    code = code + dictionary[token]
    text = text.substr(1)
  }

  debug('End encoding')

  return { encoded: code }
}

function _reverseMap (map) {
  let result = {}
  for (let [key, value] of Object.entries(map)) {
    result[value] = key
  }
  return result
}

function decodeHuffman (sequence, dictionary) {
  assert(typeof sequence === 'string', 'the first parameter must be a string')
  assert(typeof dictionary === 'object', 'the second parameter must be an object')

  debug('Start decoding')

  const reverseDictionary = _reverseMap(dictionary)

  let buffer = null
  let decoded = ''

  while (sequence.length > 0) {
    buffer = (buffer || '') + sequence[0]

    if (buffer in reverseDictionary) {
      const char = reverseDictionary[buffer]
      decoded = decoded + char
      buffer = null
    }

    sequence = sequence.substr(1)
  }

  debug('End decoding')

  return { decoded, rest: buffer }
}

module.exports = {
  createDictionary,
  encodeHuffman,
  decodeHuffman
}
