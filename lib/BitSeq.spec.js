/* eslint-env mocha */
const assert = require('assert')
const BitSeq = require('./BitSeq')

const utils = {
  bufferToBitString (buffer) {
    assert(Buffer.isBuffer(buffer))
    let result = ''
    for (let i = 0; i < buffer.length; i++) {
      result = result + buffer.readUInt8(i).toString(2).padStart(8, '0')
    }
    return result
  }
}

describe('BitSeq', () => {
  it('keeps trck of the size', () => {
    const seq = new BitSeq()
    seq.write('001')
    assert(seq.size() === 3)
    seq.write('001')
    assert(seq.size() === 6)
  })

  it('reads the bits that have been written zero-padded at the end', () => {
    const seq = new BitSeq()
    seq.write('001')

    assert.strictEqual(
      seq.size(),
      3
    )

    const buffer = seq.read()
    assert.strictEqual(
      utils.bufferToBitString(buffer),
      '00100000'
    )
  })

  it('reads the bits that have been written for the whole sequence', () => {
    const seq = new BitSeq()
    seq.write('00000001')
    seq.write('00000010')
    seq.write('0001')

    assert.strictEqual(
      seq.size(),
      20
    )

    assert.strictEqual(
      seq.toString(),
      '00000001000000100001'
    )
  })

  it('handles writing arbitrary sequences of bits', () => {
    const seq = new BitSeq()
    seq.write('11111111')
    seq.write('111')
    seq.write('111')

    assert.strictEqual(
      seq.size(),
      14
    )

    assert.strictEqual(
      seq.toString(),
      '11111111111111'
    )
  })

  it('handles writing arbitrary length of bits', () => {
    const seq = new BitSeq()
    seq.write('111111110101')

    assert.strictEqual(
      seq.size(),
      12
    )

    assert.strictEqual(
      seq.toString(),
      '111111110101'
    )
  })

  it('creates a bitSeq from a buffer', () => {
    const bitString = '110011101010011101011'
    const size = bitString.length

    const seq = new BitSeq()
    seq.write(bitString)
    const buffer = seq.read()

    assert.strictEqual(
      BitSeq.fromBuffer(buffer, size).toString(),
      bitString
    )
  })
})
