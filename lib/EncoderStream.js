/**
 * decoded a huf-encoded buffer into a text
 *
 * @param {Stream} textStream
 * @return {string}
 **/

const stream = require('stream')
const Header = require('../lib/Header')
const Dictionary = require('../lib/Dictionary')
const BitSeq = require('../lib/BitSeq')
const { decodeHuffman } = require('../lib/Encoder')

let buffer
let header
let dictionary
let restBitSequence = ''
let restCodeSizeInBits

const BYTE = 8

function _digestAndDecodeChunk (chunk, callback) {
  buffer = chunk

  if (!header) {
    if (buffer.length > Header.SIZE) {
      header = Header.parse(buffer.slice(0, Header.SIZE))

      const restBuffer = buffer.slice(Header.SIZE + 1) // discard 1 null byte
      _digestAndDecodeChunk(restBuffer, callback)
    }
  } else if (!dictionary) {
    if (buffer.length > header.dictSize) {
      // Retrieve the dictionary
      dictionary = buffer.slice(0, header.dictSize).toString('utf8')
      try {
        dictionary = Dictionary.parse(dictionary)
      } catch (err) {
        // TODO this shouldnt be needed
        console.error('Error parsing the dictionary', err)
        throw err
      }

      // initialize the length of the buffer
      const restBuffer = buffer.slice(header.dictSize)
      _digestAndDecodeChunk(restBuffer, callback)
    }
  } else {
    restCodeSizeInBits = restCodeSizeInBits || header.codeSize
    const bitSeq = BitSeq.fromBuffer(buffer, restCodeSizeInBits)

    const { decoded, rest } = decodeHuffman(restBitSequence + bitSeq.toString(), dictionary)

    restBitSequence = rest
    restCodeSizeInBits = restCodeSizeInBits - (buffer.length * BYTE)
    callback(null, decoded)
  }
}

module.exports.decodeHuffmanStream = new stream.Transform({
  transform (chunk, encoding, callback) {
    _digestAndDecodeChunk(chunk, callback)
  }
})
