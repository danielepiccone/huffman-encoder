/* eslint-env mocha */
const assert = require('assert')
const Dictionary = require('./Dictionary')

describe('Dictionary', () => {
  it('encodes a dictionary as [symbol] : [path:padding]', () => {
    const dictionary = { a: '10101', b: '00100', c: '0111' }
    assert.deepStrictEqual(
      Dictionary.create(dictionary),
      JSON.stringify(
        { a: '21:5', b: '4:5', c: '7:4' }
      )
    )
  })

  it('decodes a dictionary as [symbol] : [path:padding]', () => {
    const dictionary = JSON.stringify({ a: '21:5', b: '4:5', c: '7:4' })
    assert.deepStrictEqual(
      Dictionary.parse(dictionary),
      { a: '10101', b: '00100', c: '0111' }
    )
  })
})
