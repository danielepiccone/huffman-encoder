/* eslint-env mocha */
const assert = require('assert')
const BinaryHeap = require('./BinaryHeap')

// TODO
// A queue is empty on construction
// A queue has size 0 on construction
// After 𝑛 enqueues to an empty queue, 𝑛>0, the queue is not empty and its size is 𝑛
// If the size is 𝑛, then after 𝑛 dequeues, the queue is empty and has size 0
// If one enqueues the values 1 through 50, in some random order, into an empty queue, then if 50 dequeues are done the values dequeued are 1 through 50, respectively
// Dequeueing from an empty queue throws a NoElementError
// Peeking into an empty queue throws a NoElementError

describe('BinaryHeap', () => {
  it('should heapify the data', () => {
    assert.deepStrictEqual(
      BinaryHeap.heapify([3, 10, 7, 5, 12, 20]),
      [3, 5, 7, 10, 12, 20]
    )

    assert.deepStrictEqual(
      BinaryHeap.heapify([32, 140, 745, 415, 122, 220]),
      [32, 122, 220, 415, 140, 745]
    )
  })

  describe('#push', () => {
    it('should add an element to an already ordered heap considering its priority', () => {
      const heap = new BinaryHeap({ data: [1, 3, 4, 7, 5, 5, 8] })
      heap.push(6)
      const data = heap._data
      assert.deepStrictEqual(data, [ 1, 3, 4, 6, 5, 5, 8, 7 ])
    })

    it('should sift up the element until its priorirty', () => {
      const heap = new BinaryHeap({
        compare: (a, b) => a < b,
        data: [
          0.16, 0.16, 0.16, 0.33, 0.16
        ]
      })

      heap.push(0.33)
      const data = heap._data
      assert.deepStrictEqual(data, [
        0.16, 0.16, 0.16, 0.33, 0.16, 0.33
      ])
    })
  })

  describe('#pop', () => {
    it('should return the element with the highest priority and remove it', () => {
      const heap = new BinaryHeap({ data: [1, 3, 4, 7, 5, 5, 8] })
      assert.strictEqual(heap.pop(), 1)
    })

    it('should balance the heap', () => {
      const heap = new BinaryHeap({ data: [1, 3, 10, 7, 5, 12, 20] })
      heap.pop()
      assert.deepStrictEqual(heap._data, [3, 5, 10, 7, 20, 12])
    })

    it('should throw if the heap is empty', () => {
      const heap = new BinaryHeap()
      let hasThrown = false
      try {
        heap.pop()
      } catch (err) {
        hasThrown = !!err
      }
      assert.ok(hasThrown)
    })
  })

  describe('#peek', () => {
    it('should return the element with the highest prioriy', () => {
      const heap = new BinaryHeap({ data: [1, 3, 10, 7, 5, 12, 20] })
      assert.strictEqual(heap.peek(), 1)
    })

    it('should throw if the heap is empty', () => {
      const heap = new BinaryHeap()
      let hasThrown = false
      try {
        heap.pop()
      } catch (err) {
        hasThrown = !!err
      }
      assert.ok(hasThrown)
    })
  })

  describe('#size', () => {
    it('should return the heap size', () => {
      const heap = new BinaryHeap({ data: [1, 3, 10, 7, 5, 12, 20] })
      assert.strictEqual(heap.size(), 7)
    })
  })

  describe('#isEmpty', () => {
    it('should return true if the heap is empty', () => {
      const heap = new BinaryHeap({ data: [] })
      assert.strictEqual(heap.isEmpty(), true)
    })

    it('should return false if the heap is not empty', () => {
      const heap = new BinaryHeap({ data: [1, 2] })
      assert.strictEqual(heap.isEmpty(), false)
    })
  })
})
