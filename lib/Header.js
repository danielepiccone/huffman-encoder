const assert = require('assert')
const BitSeq = require('./BitSeq')

const NUL = Buffer.alloc(1, 0)

const Header = {}

// Size in octets
Header.SIZE_SIGNATURE = 3
Header.SIZE_VERSION = 1
Header.SIZE_DICTSIZE = 4
Header.SIZE_CODESIZE = 4
Header.SIZE = 3 + 1 + 4 + 4 + 1 /* nul separator */

Header.create = function create ({ dictionary, encoded }) {
  const codeSequence = BitSeq.fromString(encoded)

  const dictionaryBuffer = Buffer.from(JSON.stringify(dictionary))

  // Consider 3 bytes for the signature
  const SIGNATURE = Buffer.alloc(3)
  SIGNATURE.write('HUF')

  // Consider 1 byte for the version
  const VERSION = Buffer.alloc(1)
  VERSION.writeUInt8(0x01)

  // Consider 4 bytes for the dictionary size
  const dictSize = Buffer.alloc(4)

  // Consider 4 bytes for the content size (TODO)
  const codeSize = Buffer.alloc(4) // use uint32

  dictSize.writeUInt32BE(dictionaryBuffer.length, 0)
  codeSize.writeUInt32BE(codeSequence.size(), 0)

  return Buffer.concat([ SIGNATURE, VERSION, dictSize, codeSize, NUL ])
}

Header.parse = function parse (buffer) {
  assert(Buffer.isBuffer(buffer), 'Trying to parse an invalid buffer')

  // Check the signature
  const signature = buffer.slice(0, 3).toString('utf8')

  assert(signature === 'HUF', 'Trying to process an invalid file')

  // Parse the header
  const version = buffer.slice(3, 4).readUInt8()
  const dictSize = buffer.slice(4, 8).readUInt32BE()
  const codeSize = buffer.slice(8, 12).readUInt32BE()

  // Check the null separator
  assert.ok(buffer.slice(12, 13).compare(NUL) === 0)

  return { version, dictSize, codeSize }
}

module.exports = Header
